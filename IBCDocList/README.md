EVCIO
------

### EVCIO Technical White Paper

- [English](EVCIOTechnicalWhitePaper.md)
- [中文](EVCIOTechnicalWhitePaper_zh.md) 

EVCIO bases on EOSIO, so you can also referer EOS.IO Technical White Paper:  
- [English](https://github.com/EOSIO/Documentation/blob/master/TechnicalWhitePaper.md)
- [Russian](https://github.com/EOSIO/Documentation/blob/master/ru-RU/TechnicalWhitePaper.md) translated by [@blockchained](https://steemit.com/@blockchained)
- [Chinese](https://github.com/EOSIO/Documentation/blob/master/zh-CN/TechnicalWhitePaper.md) translated by [@dayzh](https://steemit.com/@dayzh)
- [Korean](https://github.com/EOSIO/Documentation/blob/master/ko-KR/TechnicalWhitePaper.md) translated by [@clayop](https://steemit.com/@clayop)


### IBC docs
- [EOSIO IBC Priciple and Design](IBC/EOSIO_IBC_Priciple_and_Design.md)
- [EOSIO IBC Priciple and Design 中文](IBC/EOSIO_IBC_Priciple_and_Design_zh.md)
- [IBC User Guide](IBC/README.md)
- [Token Registration and Management](IBC/Token_Registration_and_Management.md)
- [IBC System Deployment](IBC/IBC_System_Deployment.md)

### EVCIO Batch-PBFT / LIB Acceleration Solution docs
- [EVCIO LIB Acceleration Solution: Batch PBFT](LIB/EVCIO_Batch_PBFT_I.md)
- [EVCIO Batch-PBFT Upgrade Solution](LIB/EVCIO_Batch_PBFT_II.md)



