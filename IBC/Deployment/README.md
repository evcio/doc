# IBC Deployment Document

This article will detail how to build, deploy, and test an IBC system. Take the Kylin test network and the EVCIO test network environment as examples.


## IBC 4 libraries
```
https://github.com/evccore/evc.contract-prebuild.git
Https://github.com/evccore/ibc_plugin_eos.git
https://github.com/evccore/ibc_contracts.git
Https://github.com/evccore/ibc_plugin_evc.git
```
## The basic steps
1. build and deploy the eos version
	1. build evcibc.contracts
		1. solution 1 Pre building evcibc.contracts
		1. solution 2 building evcibc.contracts from Source code
			1. build eos
			1. build eosio.cdt
			1. build evcibc.contracts
	1. Deploy evcibc.contracts
	1. Initialize evcibc.contracts
	1. build ibc_plugin_eos
	1. Configure ibc_plugin_eos
1. build and deploy the evc version
	1. build evcibc.contracts
		1. solution 1 Pre building evcibc.contracts
		1. solution 2 building evcibc.contracts from Source code
			1. build evc
			1. build evc.cdt
			1. build evcibc.contracts
	1. Deploy evcibc.contracts
	1. Initialize evcibc.contracts
	1. build ibc_plugin_evc
	1. Configure ibc_plugin_evc
1. IBC cross-chain trading test example

## building and deploying the eos version
### building evcibc.contracts
#### solution 1 Pre-building evcibc.contracts

```
$ git clone https://github.com/evccore/evc.contract-prebuild.git
$ cd evc.contract-prebuild/ibceosio_version
```

#### solution 2 building evcibc.contracts from source code
##### building eos

```
$ git clone https://github.com/EOSIO/eos.git
$ cd eos && git checkout release/v1.5.x
$ ./eosio_build.sh
$ sudo ./eosio_install.sh
```

##### building eosio.cdt
```
$ git clone https://github.com/EOSIO/eosio.cdt.git
$ cd eosio.cdt && git checkout release/v1.4.x
$ ./build.sh
$ sudo ./install.sh
```
##### building evcibc.contracts

``` bash
$ git clone https://github.com/evccore/ibc_contracts.git
$ cd evcibc.contracts
$ ./build.sh
```

### Deploy evcibc.contracts
Create three accounts ibc2chaineos, ibc2tokeneos, ibc2relayeos<br/>
Purchase RAM 10Mb, CPU 100 EOS, NET 100 EOS for ibc2chaineos, ibc2tokeneos<br/>
Purchase CPU 5000 EOS for ibc2relayeos, NET 500 EOS<br/>
And deploy the ibc.chain contract with ibc2chaineos and the ibc.token contract with ibc2tokeneos
### Initialization evcibc.contracts

``` bash

Cleos1=cleos -u http://kylin.fn.eosbixin.com
Contract_chain=ibc2chaineos
Contract_token=ibc2tokeneos

Set the two-chain ibc2tokeneos contract to set eosio.code permissions
$cleos1 set account permission ${contract_token} active '{"threshold": 1, "keys":[{"key":"'${token_c_pubkey}'", "weight":1}], "accounts":[ {"permission":{"actor":"'${contract_token}'","permission":"eosio.code"},"weight":1}], "waits":[] }' owner -p $ {contract_token}

$cleos1 push action ${contract_chain} setglobal '[{"lib_depth":170}]' -p ${contract_chain}
$cleos1 push action ${contract_chain} relay '["add","ibc2relayeos"]' -p ${contract_chain}
$cleos1 push action ${contract_token} setglobal '["ibc2chaineos","ibc2tokeneos",5000,1000,10,true]' -p ${contract_token}
$cleos1 push action ${contract_token} regacpttoken \
    '["eosio.token","1000000000.0000 EOS","ibc2tokeneos","10.0000 EOS","5000.0000 EOS",
    "100000.0000 EOS",1000,"organization-name","www.website.com","fixed","0.1000 EOS",0.01,true,"4,EOSPG"]' -p ${contract_token}
$cleos1 push action ${contract_token} regpegtoken \
    '["1000000000.0000 EVCIOPG","10.0000 EVCIOPG","5000.0000 EVCIOPG",
    "100000.0000 EVCIOPG",1000,"ibc2tokeneos","eosio.token","4,EVCIO",true]' -p ${contract_token}

```
### building ibc_plugin_eos

```
$ git clone https://github.com/evccore/ibc_plugin_eos.git
$ cd ibc_plugin_eos
# Comment out #define PLUGIN_TEST on line 39 in the plugins/ibc_plugin/ibc_plugin.cpp file
$ ./eosio_build.sh
```
### Configuring ibc_plugin_eos

Relay full node configuration item

```
Plugin = eosio::ibc::ibc_plugin
Ibc-chain-contract = ibc2chaineos
Ibc-token-contract = ibc2tokeneos
Ibc-relay-name = ibc2relayeos
Ibc-relay-private-key = EOS5jLHvXsFPvUAawjc6qodxUbkBjWcU1j6GUghsNvsGPRdFV5ZWi=KEY:5K2ezP476ThBo9zSrDqTofzaLiKrQaLEkAzv3USdeaFFrD5LAX1
Ibc-listen-endpoint = 0.0.0.0:6001
#ibc-peer-address = 127.0.0.1:6002
Ibc-sidechain-id = aca376f206b8fc25a6ed44dbdc66547c36c6c33e3a119ffbeaef943642f0e906
Ibc-peer-private-key = EOS65jr3UsJi2Lpe9GbxDUmJYUpWeBTJNrqiDq2hYimQyD2kThfAE=KEY:5KHJeTFezCwFCYsaA4Hm2sqEXvxmD2zkgvs3fRT2KarWLiTwv71
```

## build and deploy the evc version
### building evcibc.contracts
#### solution 1 Pre-building evcibc.contracts

```
$ git clone https://github.com/evccore/evc.contract-prebuild.git
$ cd evc.contract-prebuild/ibcevc_version
```

#### solution 2 building evcibc.contracts from Source code
##### build evc
```
$ git clone https://github.com/evccore/evc.git
$ cd eos && git checkout release/v2.0.x
$ ./eosio_build.sh
$ sudo ./eosio_install.sh
```
##### building evc.cdt
```
$ git clone https://github.com/evccore/evc.cdt.git
$ cd evc.cdt && git checkout release/v2.0.x
$ ./build.sh
$ sudo ./install.sh
```
##### building evcibc.contracts

``` bash
$ git clone https://github.com/evccore/ibc_contracts.git
$ cd evcibc.contracts
$ ./build.sh
```
### Deploy evcibc.contracts
Create two accounts ibc2chainevc, ibc2tokenevc, ibc2relayevc<br/>
Buy RAM 10Mb, CPU 100EVCIO, NET 100 EVCIO for ibc2chainevc, ibc2tokenevc<br/>
Purchase CPU 5000 EVCIO for ibc2relayevc, NET 500 EVCIO<br/>
And deploy the ibc.chain contract on ibc2chainevc on each test network, and deploy the ibc.token contract on ibc2tokenevc
### Initialization evcibc.contracts

``` bash
Cleos2=cleos -u http://47.254.82.241
Contract_chain=ibc2chainevc
Contract_token=ibc2tokenevc

Ibc2tokenevc contract sets eosio.code permissions

$cleos2 set account permission ${contract_token} active '{"threshold": 1, "keys":[{"key":"'${token_c_pubkey}'", "weight":1}], "accounts":[{"permission":{"actor":"'${contract_token}'","permission":"eosio.code"},"weight":1}], "waits":[] }' owner -p ${contract_token}

$cleos2 push action ${contract_chain} setglobal '[{"lib_depth":170}]' -p ${contract_chain}
$cleos2 push action ${contract_chain} relay '["add","ibc2relayeos"]' -p ${contract_chain}
$cleos2 push action ${contract_token} setglobal '["ibc2chainevc","ibc2tokenevc",5000,1000,10,true]' -p ${contract_token}
$cleos2 push action ${contract_token} regacpttoken \
    '["eosio.token","1000000000.0000 EVCIO","ibc2tokenevc","10.0000 EVCIO","5000.0000 EVCIO",
    "100000.0000 EVCIO",1000,"organization-name","www.website.com","fixed","0.1000 EVCIO",0.01,true,"4,EVCIOPG"]' -p ${contract_token}
$cleos2 push action ${contract_token} regpegtoken \
    '["1000000000.0000 EOSPG","10.0000 EOSPG","5000.0000 EOSPG",
    "100000.0000 EOSPG",1000,"ibc2tokenevc","eosio.token","4,EOS",true]' -p ${contract_token}

```

### building ibc_plugin_evc

``` bash
$ git clone https://github.com/evccore/ibc_plugin_evc.git
$ cd ibc_plugin_evc && git checkout feature/ibc-plugin # In order to test the other functions of evc together, this branch has merged the contents of the master branch.
# Comment out #define PLUGIN_TEST on line 39 in the plugins/ibc_plugin/ibc_plugin.cpp file
$ ./eosio_build.sh
```
### Configuring ibc_plugin_evc
Relay full node configuration item

```
Plugin = eosio::ibc::ibc_plugin
Ibc-chain-contract = ibc2chainevc
Ibc-token-contract = ibc2tokeneos
Ibc-relay-name = ibc2relayeos
Ibc-relay-private-key = EOS5jLHvXsFPvUAawjc6qodxUbkBjWcU1j6GUghsNvsGPRdFV5ZWi=KEY:5K2ezP476ThBo9zSrDqTofzaLiKrQaLEkAzv3USdeaFFrD5LAX1
Ibc-listen-endpoint = 0.0.0.0:6001
#ibc-peer-address = 127.0.0.1:6002
Ibc-sidechain-id = aca376f206b8fc25a6ed44dbdc66547c36c6c33e3a119ffbeaef943642f0e906
Ibc-peer-private-key = EOS65jr3UsJi2Lpe9GbxDUmJYUpWeBTJNrqiDq2hYimQyD2kThfAE=KEY:5KHJeTFezCwFCYsaA4Hm2sqEXvxmD2zkgvs3fRT2KarWLiTwv71
```


## IBC Cross-Chain Transaction Test Example

After the configuration is started, the node is initialized, and after the parties' contracts are initialized, and the first section is completed, cross-chain transactions can be performed.

### Contract name

- eos relay contract account: ibctoken2evc
- Bos's contract account: ibctoken2eos

## Detailed operation

The urls of the two networks that need to be used:<br/>
Kylin-api= -u http://kylin.meet.one:8888<br/>
Bos-api= -u http://evc-testnet.meet.one:8888

### 1) Transfer "50.0000 EOS" from kylin test network to EVCIO test online
````
Cleos ${kylin-api} transfer ibctoken2evc "10.0000 EOS" "evccoretest2@evc notes infomation" -p ibckylintest
Cleos ${kylin-api} get currency balance eosio.token ibckylintest # reduction
Cleos ${kylin-api} get currency balance eosio.token ibctoken2evc #增
````
View on the EVCIO test online
```
$cleos ${evc-api} get currency balance ibctoken2eos evccoretest2
100.0000 EOSPG
```

### 2) From the EVCIO test network, transfer "50.0000 EVCIO" to kylin test online
```
Cleos ${evc-api} transfer evccoretest2 ibctoken2eos "50.0000 EVCIO" "evccoretest2@eos notes infomation" -p ibckylintest
Cleos ${kylin-api} transfer "10.0000 EOS"
Cleos ${evc-api} get currency balance eosio.token evccoretest2 # reduction
Cleos ${evc-api} get currency balance eosio.token ibctoken2eos #增
```
View on kylin test network
```
$cleos ${kylin-api} get currency balance ibctoken2evc ibckylintest
50.0000 EVCIOPG
```

### 3) Transfer "10.0000 EVCIOPG" from kylin test network to EVCIO test network
````
Cleos ${kylin-api} push action ibctoken2evc transfer '["ibckylintest","ibctoken2evc","10.0000 EVCIOPG" "evccoretest2@evc notes infomation"]' -p ibckylintest
Cleos ${kylin-api} get currency balance ibctoken2evc ibckylintest #min 10 EVCIOPS
````
View on the EVCIO test online
```
$cleos ${evc-api} get currency balance eosio.token evccoretest2 #增 10 EVCIO
```

### 4) Transfer "10.0000 EOSPG" from the EVCIO test network to kylin test network
````
Cleos ${evc-api} push action ibctoken2eos transfer '["evccoretest2","ibctoken2eos","10.0000 EOSPG" "ibckylintest@eos notes infomation"]' -p evccoretest2
Cleos ${evc-api} get currency balance ibctoken2eos evccoretest2 #reduce 10 EVCIOPS
````
View on kylin test online
```
$cleos ${kylin-api} get currency balance eosio.token ibckylintest #增 10 EOS
```

*Note: Due to the cross-chain transfer, there will be a delay in the arrival time*
