# EVCIO LIB Acceleration Solution
*Author:Deadlock [Github](https://github.com/qianxiaofeng)*

- [基于PBFT提升EOSIO共识速度的算法](./基于PBFT提升EOSIO共识速度的算法.md)
- [Algorithm for improving EOSIO consensus speed based on Batch-PBFT](./Algorithm_for_improving_EOSIO_consensus_speed_based_on_Batch-PBFT.md)
- [EVCIO LIB Acceleration Solution: Batch PBFT](./EVCIO_Batch_PBFT_I.md)
- [EVCIO Batch-PBFT Upgrade Solution](./EVCIO_Batch_PBFT_II.md)
